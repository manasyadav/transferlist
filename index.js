document.addEventListener("DOMContentLoaded", () => {
  let data = {
    JS: false,
    HTML: false,
    CSS: false,
    TS: false,
    React: true,
    Angular: true,
    Vue: true,
    Svelte: true,
  };

  let left = document.querySelector(".left");
  let right = document.querySelector(".right");
  let controls = document.querySelector(".controls");

  function showData() {
    left.innerHTML = "";
    right.innerHTML = "";

    for (item in data) {
      let checkbox = document.createElement("input");
      checkbox.type = "checkbox";
      checkbox.id = item;

      let label = document.createElement("label");
      label.htmlFor = item;

      let nextLine = document.createElement("br");

      label.appendChild(document.createTextNode(item));

      if (data[item] === false) {
        left.appendChild(checkbox);
        left.appendChild(label);
        left.appendChild(nextLine);
      } else {
        right.appendChild(checkbox);
        right.appendChild(label);
        right.appendChild(nextLine);
      }
    }
  }

  controls.addEventListener("click", (e) => {
    if (e.target.id === "all-left") {
      for (const item in data) {
        data[item] = false;
      }
    }
    if (e.target.id === "all-right") {
      for (const item in data) {
        data[item] = true;
      }
    }
    
    if(e.target.id === "right"){
      let leftChecked = document.querySelectorAll(".left input[type=checkbox]:checked");
      
      leftChecked.forEach((item) => {
        data[item.id] = true;
      })

    }

    if(e.target.id === "left"){
      let rightChecked = document.querySelectorAll(".right input[type=checkbox]:checked");
      
      rightChecked.forEach((item) => {
        data[item.id] = false;
      })

    }

    showData();
  });

  showData();
});
